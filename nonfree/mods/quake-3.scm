;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (nonfree mods quake-3)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((guix-gaming licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system trivial)
  #:use-module (guix-gaming build-system quake-3)
  #:use-module (gnu packages compression))

;; REVIEW: There is probably no need to propagate quake-3-arena just as Emacs
;; packages don't propagate Emacs.
;; TODO: Add https://lvlworld.com/ importer.

(define-public quake-3-xcsv-hires
  (package
    (name "quake-3-xcsv-hires")
    (version "2010")
    (source
     (origin
       (method url-fetch/zipbomb)
       ;; TODO: sha256 does not seem to be stable.
       (uri "http://ioquake3.org/files/xcsv_hires.zip")
       (sha256
        (base32
         ;; "02vjay7pwffqqkr17v3a3hirwpyw8hnmkvinns69pi953hvrk477"
         "0kapjgxwgjq4sca1vgcsxj8qs9xgnxjpi1dfi68z06i5zsmfxzx1"))))
    (build-system quake-3-build-system)
    (synopsis "High-resolution Quake 3 replacement texture pack")
    (description "These textures are for use with the Quake 3 Arena engine,
including IoQuake3, and any mods.  They are designed to replace the standard
Baseq3 textures that shipped with Quake3.

98% of the textures are higher resolution than the originals, so for example a
256x256 texture is now 512x512.  This does not change the size they appear on
pre-compiled maps.  If you are using them to create new maps, then you are
advised to 1/4 scale the textures to match the original size, as most of the
original maps use a 0.5 scaling on the texture.  So you would set your width
height to 0.25 to get the best scaling.  Some textures just did not warrant
being higher res because they would be massive in size.  Not all textures are
exact replicas of the original, but hopefully they've been given justice where
needed.

These textures were originally created to distribute XCSV as a standalone game
and still include the Baseq3 textures.  This means that any 3rd party maps that
use Baseq3 textures will not have missing textures.")
    (home-page "https://ioquake3.org/extras/replacement_content/") ; Original http://www.xcsv.net/hires/ seems dead.
    (license license:cc-by-nc-nd4.0)))

(define-public quake-3-toxicmetal
  (package
    (name "quake-3-toxicmetal")
    (version "20190512")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/s-z/scorn-toxicmetal.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "069j3sglrljr5q44q7yclds7ssy8vc1gwzc26hhpxfsxcqxd52q2"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "This map will take you into a industrial environment where
uranium is used to kill virus.  Ground area is toxic so be aware of toxic
hazards...  Try to reach the center of the map to get the Quad Damage.  You can
also move around the map while being on a storage train.")
    (home-page "https://lvlworld.com/review/id:2385")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-terminatria
  (package
    (name "quake-3-terminatria")
    (version "20110813")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/a-f/bst3dm1.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "13l8lzqwv7928f367g4bngrk7pak06wyhqignypz0lad9m0w3w4k"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Entry into the Maverick Servers and Gaming Mapping Competition
#3 for Quake 3 Arena.")
    (home-page "https://lvlworld.com/review/id:2208")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-solitude
  (package
    (name "quake-3-solitude")
    (version "20101023")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/s-z/solitude.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "0wswh5l2cp5rms20vs703ynm4fpklc7brnvdbq4c4q21l47gqjwl"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Final version for Mavericks Summer 2010 Competition.")
    (home-page "https://lvlworld.com/review/id:2126")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-achromatic
  (package
    (name "quake-3-achromatic")
    (version "20110730")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/q3gwdm1.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1gjkgch94xg7y3qv39z80vdc21nv4d3n2k060n0khgddvy9p4d0y"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "FFA map for Quake 3 Arena")
    (home-page "https://lvlworld.com/review/id:2178")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-derwylls-castle-2
  (package
    (name "quake-3-derwylls-castle-2")
    (version "20100925")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/nor3ctf1.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1qf1qmiawbg2qdrfx4mdyyzv2h9bpsv3xhlnw7387carp0clwn70"))))
    (build-system quake-3-build-system)
    (synopsis "TDM/CTF map for Quake 3 Arena")
    (description "The old muntain castle on the moon called Muriest Darm
orbiting around its planet Derwyllus.  It was built by unknown civilisation and
it served as some kind of gate into something what is under the surface of whole
asteroid.  Archeological studies are necessary.

It is only one of the three months orbiting around this planet and discovered so
far.

On the second moon called Nigror is a human colony.")
    (home-page "https://lvlworld.com/review/id:2142")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-rustgrad
  (package
    (name "quake-3-rustgrad")
    (version "20141119")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/rustgrad.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "01k81f2fyjp2prg1hr035qhc9lzl74s9cvlfdaza2qgw1590l8km"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/TDM map for Quake 3 Arena")
    (description "FFA/TDM map for Quake 3 Arena.")
    (home-page "https://lvlworld.com/review/id:2291")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-geotechnic
  (package
    (name "quake-3-geotechnic")
    (version "20150915")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/phantq3dm6_mc.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "0p9w02bm681hn2658d434q81x71700snyaz8c5ffn9d66x2zcd21"))))
    (build-system quake-3-build-system)
    (synopsis "TDM map for Quake 3 Arena")
    (description "Geotechnic was created for the Mapcore Quake 3 15th
Anniversary Contest held between October 8 and December 3, 2014.  It won first
place.

This is the final update for the contest version of this map, with a few fixes
and changes inspired by feedback and judging.")
    (home-page "https://lvlworld.com/review/id:2314")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-corrosion
  (package
    (name "quake-3-corrosion")
    (version "20101024")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/phantq3dm3_rev.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "0hb1h51apnmr4l5593knq0b9dy7ki0valf1wianss5ivwym0wsks"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/TDM map for Quake 3 Arena")
    (description "Corrosion won second place in the Maverick Servers Mapping
Competition #2, held in the Summer of 2010

This version represents the map included in the Maverick Servers Final Map Pack,
minus the sponsor logos.  Many improvements were made since the initial contest
submission.")
    (home-page "https://lvlworld.com/review/id:2131")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-server-overload
  (package
    (name "quake-3-server-overload")
    (version "20110808")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/q3ctfp22_final.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "03ifr82i4dwphfxlnqjc1bq6y3y0jpb8iw48s65lrkwi2jxw96ay"))))
    (build-system quake-3-build-system)
    (synopsis "CTF map for Quake 3 Arena")
    (description "CTF map for Quake 3 Arena.")
    (home-page "https://lvlworld.com/review/id:2365")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-forttown
  (package
    (name "quake-3-forttown")
    (version "20151120")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/q3map_forttown.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1dh2dj3x2xlfdlvv9ccs0ks5n15qjrdmd96wl270id4qm01hmy48"))))
    (build-system quake-3-build-system)
    (synopsis "Small CTF and DM maps in medieval style")
    (description "There's two clans fighting each other for the fully controll
of the fort.  The -Come-A-Lot- clan and The -Still Stayers- clan.  Originally
the fort was in the Still Stayers hands, but after their leaders death, the
clans power got swayed.  The -Come-A-Lot- clan saw this, and tries to end their
weak ruling above the fort.

Now it's your turn to choose between them. Defend or conquer...?")
    (home-page "https://lvlworld.com/review/id:2330")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-rustarium
  (package
    (name "quake-3-rustarium")
    (version "20190512")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/s-z/scorn-rustarium.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1zp594j9shgshqx1xzlw8ymvj5s220rprl0fbb1mi7xsb4crsjjk"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "You are in an rusty-metal enclave, in some sort of an oil
refinery with gigantic structurs.  Standing on the dirty ground, you see upon
you a train full of metal gear, a drone is flying by and... also stops a few
times.  You are surrounded by titan wall, impossible to escape, now you have to
face the sad truth: the only way to escape this black-oil ocean and this rusty
enclave is to fight and win.  Be aware that behind pipe lines you will find
extra crispy stuffs, mega health, so don't be affraid of the dark.  Remember to
take a look at the top structure: there can be another challenger hiding there.

Welcome into this junk world created for you: be a warrior in a rusty enclave,
if you win you will be free... but if you die you will rest with the dirt and be
forgoten.

Now go, fight.")
    (home-page "https://lvlworld.com/review/id:2386")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-unholy-sanctuary
  (package
    (name "quake-3-unholy-sanctuary")
    (version "20190512")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/q3map_unholysanctuary_v1d.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "0i8gpq0g7mkqzkgzx465f4xz70mznq53v23aml1rzirppg23vp0w"))))
    (build-system quake-3-build-system)
    (synopsis "Pack of maps and characters for Quake 3 Arena")
    (description "After a long period of time, our solar system's 10th planet
called Nibiru, appeared again!  UAC (Union Aerospace Corporation from Doom
histories) noticed the planet, which came from the OORT clodus.  They mandated
John Kaleb Admiral, to discover the planet on the deck of UAC-Curiosity III
spaceship.  UAC also mandated Colonel Janella Sabando to supervision the
discovery action.  On the way to Nibiru, Admiral John caught familiar signals
which look like emergency signals form the stroggs?

After years of research, the communication between the UAC and the spaceship's
crew ended quickly with a danger signal.  After the events, the UAC won't send
another spaceship there. They concentrated to the strogg signals.  How they
could reach there and from where?  So they lead to Mars again, to find clues.
In a short time of search, found a way portal, which leads to somewhere. Maybe
to Nibiru?

When the portal's identity became known, some corporations with UAC, hired
adventurers, to continue the discovery.  In the danger signal appears repeatedly
the \"DEMON\" word.  Maybe there is a strong and dark unknow (demon)force, which
got crazed the whole planet in the past?  This question will be
unanswered. Nibiru is long time (thousands of years) not occupied, nobody could
answer for that question...

Your quest as an adventurer is to discover that unknown force, and collet the
CUBE'S, with it's rewards.")
    (home-page "https://lvlworld.com/review/id:2326")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-blackhurricane
  (package
    (name "quake-3-blackhurricane")
    (version "20170900")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/q3oamap_blackh.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1hiv4qsqdna6pznvdl592h8pdplscqz28092bryil9bqwiyr39z7"))))
    (build-system quake-3-build-system)
    (synopsis "Medium sized FFA/TDM map, above the skies, on a flying pirate ship")
    (description "This map is about a flying pirateship above the skies, with no
captain on it.  Every ship needs a captain.  A ship is nothing without a
captain!  The crew members needs to elect someone to be the captain.  But who
would be suitable enough for this task?  The answer is: The strongest?  The
wisest?  NO!  That will be the captain who leads the score board after a bloody
deathmach (or team deathmach)!

Aye Aye, Captain!

What happened with the previous captain?  I don't know.  Maybe he/she doesn't
score on the top of the scoreboard...")
    (home-page "https://lvlworld.com/review/id:2369")
    (license ((@@ (guix licenses) license) "Custom (Martinus)"
              "No URL" ""))))

(define-public quake-3-q3deck
  (package
    (name "quake-3-q3deck")
    (version "1.2")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/q3deck_final.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "0z6dkijsr0v3xiggx7hc70scgm05k4hq3x6h4a3p9viv118drj64"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Based on Deck16 and Deck17 from Unreal Tournament and UT2004
game.")
    (home-page "https://lvlworld.com/review/id:1866")
    (license ((@@ (guix licenses) license) "Custom (kixu)"
              "http://kixu.f2o.org/" ""))))

(define-public quake-3-morpheusx
  (package
    (name "quake-3-morpheusx")
    (version "20000113")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/morpheusx.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "10zxi673kshmqq01f7dz0np9h1lmpfpv2qryg2qs9ldcghz85rg9"))))
    (build-system quake-3-build-system)
    (synopsis "Three towers in space")
    (description "Based on the Unreal Tournament level Morpheus,  it's basically
three towers in space, with a small lower platform in the middle.  In quarter
gravity...

It's not a literal copy, the level plays differently, hopefully for the
better.  The physics are completely different for the two games, hence the slight
change in dimensions.

This package includes two versions, one with quarter gravity, the other with
normal gravity and every player spawning with the Grappling Hook. Suitable for
up to 8 players, if you want a pretty hectic match.  More if you're really
evil.")
    (home-page "https://lvlworld.com/review/id:56")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-endurance
  (package
    (name "quake-3-endurance")
    (version "20141202")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/q3gwdm2.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "01yxvv656x1q16yc4m5pz6z96xi7hpv9yqnm4r7fy486iqb85bxk"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Meet another masterpiece by Greg Ward (flipout).  Style and
atmosphere of the map are close to Quake 4 multiplayer.  There is almost nothing
from the original good old Quake 3: all the textures and models are redesigned,
but it rather brings benefits.  The only thing which recalls Quake 3 is its
large emblem - and the raw game play.")
    (home-page "https://lvlworld.com/review/id:2311")
    (license ((@@ (guix licenses) license) "Custom (flipout)"
              "No URL" ""))))

(define-public quake-3-trespass
  (package
    (name "quake-3-trespass")
    (version "201508")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/s-z/trespass.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1fkwm2vcy3pdnric8rsn5rbsi05ckv1385psrr5jfxn2yrs8swc8"))))
    (build-system quake-3-build-system)
    (synopsis "Abandoned factory FFA map for Quake 3 Arena")
    (description "Trespass is set in an old ammonia Factory in Germany that was
abandoned after WW2.  The place deteriorated in peace for many years until one
day when Sarge and Keel rediscovered it on a weekend urban exploration trip.
They transformed it into an illegal dueling arena which is now notorious for
attracting the shadiest, most degenerate Quake duelers and aggressive play.  Be
advised: trespassers will be shot on sight!")
    (home-page "https://lvlworld.com/review/id:2323")
    (license ((@@ (guix licenses) license) "Custom (Par Howard)"
              "No URL" ""))))

(define-public quake-3-you-ll-shoot-your-eye-out
  (package
    (name "quake-3-you-ll-shoot-your-eye-out")
    (version "2")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/g-l/lun3dm5.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1v5ip6j08a46i1rd77qs4arh9ds1mrdjpr1raqf6likrfqgw6cxw"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/Tourney map for Quake 3 Arena")
    (description "Every now and then a Quake 3 map is released that pushes the
idea of Quake 3 level editing beyond everything else that has been released to
date.  This is one of those releases.  It is very different from anything else
you have seen in Quake 3.  On a technical level, the construction of this
release is rather complex.")
    (home-page "https://lvlworld.com/review/id:2238")
    (license ((@@ (guix licenses) license) "Custom"
              "No URL" ""))))

(define-public quake-3-the-edge-of-forever
  (package
    (name "quake-3-the-edge-of-forever")
    (version "20100312-2")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/moteof_final2.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1z3zgynzvyp6z0d0pw4pf8d6w0l6238b8sx8hbmzh1krsx35disy"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Something a little different from the norm, this map proposes
a series of challenges through what must be described as a true masterpiece of a
map.  The architecture is brilliant, textures are as good as you could possibly
get and the level of detail is just mind-boggling.  Specially-made sounds are
included and the lighting sets the mood nicely.

Instead of the usual multiplayer mayhem, this map takes you into a
single-player, gothic/fantasy type world where your objective is to work your
way through the place finding 10 skull buttons to shoot in order to open an
entrance to another world.  Expect numerous switches to push and sequences to
crack in order to progress through the game.

Please refer to the website for the latest information:
http://www.simonoc.com/pages/design/maps_q3/moteof.htm.")
    (home-page "https://lvlworld.com/review/id:2091")
    (license ((@@ (guix licenses) license) "Custom (sock)"
              "No URL" ""))))

(define-public quake-3-pyramid-of-the-magician
  (package
    (name "quake-3-pyramid-of-the-magician")
    (version "20070630-9")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/pom_bots.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1dq80f4m3wqd29kawxcpdxl02y6my6n6yfxs6xplm3pl5syxgilg"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/Tourney map for Quake 3 Arena")
    (description "Pyramid Of The Magician started out as an experiment and grew
into a visually amazing, enjoyable DM and Tourney level for human players.")
    (home-page "https://lvlworld.com/review/id:1743")
    (license ((@@ (guix licenses) license) "Custom (sock)"
              "No URL" ""))))

(define-public quake-3-windsong-keep
  (package
    (name "quake-3-windsong-keep")
    (version "20110918")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/phantq3dm4.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "0xsflsbcpbg6ygg7kdq3rrpl0fx7l181z6w9rh6yvwlh1j07cjr6"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/Tourney map for Quake 3 Arena")
    (description "Windsong Keep was an entry into the Maverick Servers and
Gaming Mapping Competition #3 for Quake 3 Arena, held in the Summer of 2011,
where it placed second.

This final map has been edited slightly from the competition entry and the
sponsor logos have been removed.")
    (home-page "https://lvlworld.com/review/id:2190")
    (license ((@@ (guix licenses) license) "Custom (Tom Perryman)"
              "No URL" ""))))

(define-public quake-3-shibam
  (package
    (name "quake-3-shibam")
    (version "1.3")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/s-z/shibam_v1.3.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "0b6xiw4p8kl693xnblk1cpcsq5j1b85ykyg438bjvvjnx4bzs5q4"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/TDM map for Quake 3 Arena")
    (description "Shibam's name and look are based on a desert city in Yemen;
but some high-tech elements can also be found.  Two goals for this map: good
gameplay and turning away from the standard Quake 3 look.  Because of this,
Shibam has much more vertical gemeplay compared to the author's previous maps.
There are endless possibilities to get from A to B and as few borders as
possible - almost every visible place can be reached.")
    (home-page "https://lvlworld.com/review/id:2148")
    (license ((@@ (guix licenses) license) "Custom (Tom Perryman)"
              "No URL" ""))))

(define-public quake-3-the-forlorn-hope
  (package
    (name "quake-3-the-forlorn-hope")
    (version "20040604")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/g-l/jof3dm2.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1h9hhlj4sk3jb19khsbfgl7b2r11g7xs2msvlar2f3wakf1siv30"))))
    (build-system quake-3-build-system)
    (synopsis "Tourney map for Quake 3 Arena")
    (description "Small, fast paced tourney map with a dark medial evil theme.")
    (home-page "https://lvlworld.com/review/id:1757")
    (license ((@@ (guix licenses) license) "Custom (Sock)"
              "No URL" ""))))

(define-public quake-3-the-vast-and-furious-13th-rmx
  (package
    (name "quake-3-the-vast-and-furious-13th-rmx")
    (version "20020515")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/map-13vast.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1rlzgnbsg403i670235l0ndxpwbh3yj05npzqgz11vnw5qx2p9sk"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/TDM/CTF map for Quake 3 Arena")
    (description "Remix of the CTF Spacemap \"The Fast And Furious\" by ButterB.")
    (home-page "https://lvlworld.com/review/id:1807")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-aeon-s-dm7-redux
  (package
    (name "quake-3-aeon-s-dm7-redux")
    (version "20020515")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/map-aedm7.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1ci2haz8127f0312i0bzjlz45wp1z5yq66ch7wvyp3hsv0kq2a9z"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Very vaguely inspired by q3dm7, using a design strongly
inspired by q4dm1, with custom texture set, custom sounds and plant models.")
    (home-page "https://lvlworld.com/review/id:2225")
    (license ((@@ (guix licenses) license) "Custom (AEon)"
              "No URL" ""))))

(define-public quake-3-dreamscape
  (package
    (name "quake-3-dreamscape")
    (version "20081210")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/map-13dream.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1ifg3q83s78nyhhhm14ln191gmbad2l1zghz7dphwhi4912qkq5v"))))
    (build-system quake-3-build-system)
    (synopsis "CTF/Proball map for Quake 3 Arena")
    (description "CTF/Proball map with several floors surrounded by walls and
some void.")
    (home-page "https://lvlworld.com/review/id:2042")
    (license ((@@ (guix licenses) license) "Custom (AEon)"
              "No URL" ""))))

(define-public quake-3-darkness-awaits
  (package
    (name "quake-3-darkness-awaits")
    (version "1.1")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/s-z/twq3tourney2.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1r0157lbxalwjix3vdq5vd69yzbmr5f3mw180zmd8vvpl0lkk6vk"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/Tourney map for Quake 3 Arena")
    (description "This is a small map suited to tourney or small FFA games.  2
or 3 players recommended, more will be very chaotic (not to mention cause
telefrags -- there's only a handful of spawn points).  The map is clean and
modern looking, using Evil Lair's dsi textures, and has an atrium and some small
side areas and corridors.

Weapons available include the shotgun, grenade launcher, rocket launcher,
lightning gun, and plasma gun.  A pack or two of ammo for each are to be had,
along with some health and armor.  In FFA games, there is also a Quad; one armor
is red and the other yellow.  In tournament games both armors are yellow and the
Quad is replaced with megahealth.")
    (home-page "https://lvlworld.com/review/id:1996")
    (license ((@@ (guix licenses) license) "Open Game Source License"
              "http://lvl.sourceforge.net/lvl-ogsl.php" ""))))

(define-public quake-3-graviton
  (package
    (name "quake-3-graviton")
    (version "20041021")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/redq3dm7.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1xg6blaglwyjfc75a1ni9mybb2drlm4vcvcrar5i2xwni14vpi9c"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Map presentation fits the alien contact theme.  The textures
are not very detailed, but the geometry is used in a way that works with this
excellently.  Lighting is far from flat, and looks realistic because most of it
looks as though it is coming from textures looking like light sources.  Music
also supports the theme; a fabulous track by SonicClang really does sound alien
like, and is worth listening to even if you only play it once from the pack file
instead of in game.")
    (home-page "https://lvlworld.com/review/id:1747")
    (license ((@@ (guix licenses) license) "Custom"
              "No URL" ""))))

(define-public quake-3-the-lost-yard
  (package
    (name "quake-3-the-lost-yard")
    (version "20121221")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/map-13yard.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "13s5rvpmy8vav35mx8y6r0zkxj7szi8a7hd3hlbnrga7nz2fjccp"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/TDM map for Quake 3 Arena")
    (description "Quake1 styled version of q3dm17 \"The Longest Yard\".")
    (home-page "https://lvlworld.com/review/id:2242")
    (license ((@@ (guix licenses) license) "Custom"
              "No URL" ""))))

(define-public quake-3-the-very-end-of-reality
  (package
    (name "quake-3-the-very-end-of-reality")
    (version "20140903")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/s-z/vmpteam9.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "0r8dzqsa6dm0v71h312hb16qbld7i34kx9w8ycg93w1qh5la18vw"))))
    (build-system quake-3-build-system)
    (synopsis "CTF map for Quake 3 Arena")
    (description "A variant of Quake III Team Arena's mpteam9, much more
symmetrical.")
    (home-page "https://lvlworld.com/review/id:2242")
    (license ((@@ (guix licenses) license) "Custom"
              "No URL" ""))))

(define-public quake-3-de-dust2
  (package
    (name "quake-3-de-dust2")
    (version "201207015")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/oxodm1.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1q89rb0xc66ih1hkqwxxzbqggp75kl4zivd75z2blzwrrvyrrs6l"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Based on possibly one of the most well known game maps ever,
Dust 2 from Counter-Strike.")
    (home-page "https://lvlworld.com/review/id:2231")
    (license ((@@ (guix licenses) license) "No license"
              "No URL" ""))))

(define-public quake-3-deep-purple
  (package
    (name "quake-3-deep-purple")
    (version "20160912")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/a-f/akutatourney6.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1ph81mvdyz0234v79vjd8ag8hmklhxwsrd0xw4b5pxj43irag4ig"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Remake of the original ID map: Space Chamber (q3dm18). Always
thought this map was pretty kool, but always thought it was cluttered and the
pitfalls created too many disruptions in game flow.  Pitfalls have been removed
and the layout adjusted but staying similiar to the original map.")
    (home-page "https://lvlworld.com/review/id:2346")
    (license ((@@ (guix licenses) license) "Custom (Akuta)"
              "No URL" ""))))

(define-public quake-3-bones-and-pieces-of-eight
  (package
    (name "quake-3-bones-and-pieces-of-eight")
    (version "20040320")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/qisland.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1ax8mn6s7wd3nrnyd3i4a5pgqxqb5jcbn3zmp8k67jkipg1vvspl"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "This Island was in pre-production when the author saw the
tiny 'cache' island in the Disney movie \"Pirates of the Caribbean\" which was
the inspiration for this Island's theme.  The cursed gold which kept the
pirates, in the movie, from their eternal rest is featured here in the treasure
chest.  As a side note, the gold was fashioned from photographs of real Spanish
\"Pieces of Eight\" which were retrieved from a sunken treasure ship.  The
island itself was created using a relief map of The Big Island of Hawaii,
although it is a much smaller version.")
    (home-page "https://lvlworld.com/review/id:1761")
    (license ((@@ (guix licenses) license) "Custom (Larry Crandall)"
              "No URL" ""))))

(define-public quake-3-core-gravity-drive
  (package
    (name "quake-3-core-gravity-drive")
    (version "20160805")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/a-f/akutatourney5.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1c1qq5n5j0smmzym8vqywywx6cxywzhwkaw1q6d3k1wiibrk1cmm"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "Futuristic human exploration spaceship that has the ability to
fold space-time.  Map inspired by movie: Event Horizon Not exactly like the ship
in the movie, but similiar.  Gravity Drive does not have three magnetic rings
but only two because three interfered with gameplay.")
    (home-page "https://lvlworld.com/review/id:2342")
    (license ((@@ (guix licenses) license) "Custom (Akuta)"
              "No URL" ""))))

(define-public quake-3-poolshark
  (package
    (name "quake-3-poolshark")
    (version "20040120")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/s-z/stchdm1.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "0l1nbgl3i37yhb3qb9spabaspkh84iridsmlzcl21irncrqn4paj"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/TDM map for Quake 3 Arena")
    (description "This map was inspired by playing Deathroom Bathroom by
Requiem, ENTE's Padkitchen and MopAn's DMRoom, and is a vastly oversized pool
room, complete with table, bookshelves, comfy chairs, lamps, a grandfather clock
and a bar.  And it really is huge.  Every item available in vQ3A is featured,
all weapons, powerups and holdables.")
    (home-page "https://lvlworld.com/review/id:1735")
    (license ((@@ (guix licenses) license) "Custom (seremtan)"
              "No URL" ""))))

(define-public quake-3-egyptian-outpost
  (package
    (name "quake-3-egyptian-outpost")
    (version "20011221")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/outpost.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "1gk2w9fyhxw6ik5dl4i6w613l3ql3q7ja1drqvsm83xxzxml4rhi"))))
    (build-system quake-3-build-system)
    (synopsis "FFA/TDM map for Quake 3 Arena")
    (description "A very large Egyptian theme map.  A semi-open courtyard plays
home to a quad damage, while the four compass corners hold massive temples,
pyramid structures and edifices that are honeycombed with corridors and sunken
hallways.  A lot of action will be played out in those sunken Egyptian by-ways
since they hold the BFG and the Battle Suit.  The rest of the action is around
the Quad!")
    (home-page "https://lvlworld.com/review/id:1281")
    (license ((@@ (guix licenses) license) "Custom"
              "No URL" ""))))

(define-public quake-3-null-and-void
  (package
    (name "quake-3-null-and-void")
    (version "20100823")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri "http://files.lvlworld.com/q3a/m-r/q3dmp14.zip")
       (file-name (string-append name "-" version ".zip"))
       (sha256
        (base32 "11acmwd00nd9aqmfa4zvwdnv6dwr3vs1yqcjml50z99i065limmj"))))
    (build-system quake-3-build-system)
    (synopsis "FFA map for Quake 3 Arena")
    (description "A medium-sized space map with a unique theme.  The level has a
sci-fi or futuristic atmosphere, with glass, metal and concrete floating around
in the middle of an incredible sky box by Hipshot.")
    (home-page "https://lvlworld.com/review/id:2130")
    (license ((@@ (guix licenses) license) "Custom (fKd)"
              "No URL" ""))))
