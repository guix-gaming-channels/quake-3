;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-gaming licenses)
  #:use-module (guix licenses)
  #:export (cc-by-nc-nd4.0))

(define cc-by-nc-nd4.0
  ((@@ (guix licenses) license) "CC-BY-NC-ND 4.0"
        "https://creativecommons.org/licenses/by-nc-nd/4.0/"
        "Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International"))
