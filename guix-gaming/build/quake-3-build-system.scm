;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-gaming build quake-3-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module (guix build utils)
  #:use-module (ice-9 match)
  #:use-module (ice-9 ftw)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (%standard-phases
            quake-3-build))

;; Commentary:
;;
;; Builder-side code of the standard build procedure for Quake 3 packages.
;;
;; Code:

(define* (install #:key name outputs quake-directory #:allow-other-keys)
  (let* ((out (assoc-ref outputs "out"))
         (dir (string-append out "/bin/" quake-directory))
         (basename (string-drop name
                                (string-prefix-length name "quake-3-")))
         (doc (string-append out "/doc/share/quake-3/" basename)))
    (mkdir-p dir)
    (mkdir-p doc)
    (for-each (cut install-file <> dir)
              (find-files "." "\\.pk3$"))
    (for-each (cut install-file <> doc)
              (find-files "." "\\.txt$")))
  #t)

(define %standard-phases
  (modify-phases gnu:%standard-phases
    (delete 'bootstrap)
    (delete 'configure)
    (delete 'build)
    (delete 'check)
    (replace 'install install)))

(define* (quake-3-build #:key inputs (phases %standard-phases)
                        #:allow-other-keys #:rest args)
  "Build the given Quake 3 mod, applying all of PHASES in order."
  (apply gnu:gnu-build #:inputs inputs #:phases phases args))

;;; quake-3-build-system.scm ends here
