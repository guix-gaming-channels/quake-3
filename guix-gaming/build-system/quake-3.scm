;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-gaming build-system quake-3)
  #:use-module (guix store)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix derivations)
  #:use-module (guix search-paths)
  #:use-module (guix build-system)
  #:use-module (guix build-system gnu)
  #:use-module (guix svn-download)
  #:use-module (ice-9 match)
  #:export (%quake-3-build-system-modules
            lower
            quake-3-build
            quake-3-build-system))

;; Commentary:
;;
;; Standard build procedure for Quake 3 mods.
;;
;; Code:

(define %quake-3-build-system-modules
  ;; Build-side modules imported by default.
  `((guix-gaming build quake-3-build-system)
    ,@%gnu-build-system-modules))

(define* (lower name
                #:key
                source inputs native-inputs outputs
                system target
                #:allow-other-keys
                #:rest arguments)
  "Return a bag for NAME."
  (define private-keywords
    '(#:source #:target #:inputs #:native-inputs))

  (bag
    (name name)
    (system system)
    (host-inputs `(,@(if source
                         `(("source" ,source))
                         '())
                   ,@inputs

                   ;; Keep the standard inputs of 'gnu-build-system', mostly for
                   ;; decompression.
                   ,@(standard-packages)))
    (build-inputs `(,@(let ((compression (resolve-interface '(gnu packages compression))))
                        (map (match-lambda
                               ((name package)
                                (list name (module-ref compression package))))
                             `(("unzip" unzip))))
                    ,@native-inputs))
    (outputs outputs)
    (build quake-3-build)
    (arguments (strip-keyword-arguments private-keywords arguments))))

(define* (quake-3-build store name inputs
                        #:key
                        (quake-directory "baseq3")
                        (phases '(@ (guix-gaming build quake-3-build-system)
                                    %standard-phases))
                        (outputs '("out"))
                        (search-paths '())
                        (system (%current-system))
                        (guile #f)
                        (substitutable? #t)
                        (imported-modules %quake-3-build-system-modules)
                        (modules '((guix-gaming build quake-3-build-system)
                                   (guix build utils))))
  "Build SOURCE with INPUTS."
  (define builder
    `(begin
       (use-modules ,@modules)
       (quake-3-build #:name ,name
                      #:source ,(match (assoc-ref inputs "source")
                                       (((? derivation? source))
                                        (derivation->output-path source))
                                       ((source)
                                        source)
                                       (source
                                        source))
                      #:quake-directory ,quake-directory
                      #:system ,system
                      #:phases ,phases
                      #:outputs %outputs
                      #:search-paths ',(map search-path-specification->sexp
                                            search-paths)
                      #:inputs %build-inputs)))

  (define guile-for-build
    (match guile
      ((? package?)
       (package-derivation store guile system #:graft? #f))
      (#f                               ; the default
       (let* ((distro (resolve-interface '(gnu packages commencement)))
              (guile  (module-ref distro 'guile-final)))
         (package-derivation store guile system #:graft? #f)))))

  (build-expression->derivation store name builder
                                #:inputs inputs
                                #:system system
                                #:modules imported-modules
                                #:outputs outputs
                                #:guile-for-build guile-for-build
                                #:substitutable? substitutable?))

(define quake-3-build-system
  (build-system
    (name 'quake-3)
    (description "The build system for Quake 3 packages")
    (lower lower)))

;;; quake-3.scm ends here
